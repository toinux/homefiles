{
  description = "My home configuration, ";

  # inputs = {
  #   nixpkgs = {
  #     url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  #   };

  #   home-manager = {
  #     url = "github:nix-community/home-manager";
  #     inputs.nixpkgs.follows = "nixpkgs";
  #   };
  # };

  outputs = {...}: {
    # defaultPackage.x86_64-linux = home-manager.defaultPackage.x86_64-linux;
    # formatter.x86_64-linux = nixpkgs;

    lib = {
      mkHomeConfiguration = userName: homeDir: imports: (
        let
          homeConf = {
            # TODO: please change the username & home directory to your own
            home.username = userName;
            home.homeDirectory = homeDir;

            imports = [./home/configuration.nix];
          };
        in
          homeConf // {imports = (homeConf.imports or []) ++ imports;}
      );
    };
  };
}
