{ pkgs, lib, ... }:

let
  configFile = ".ssh/config";
  targetFile = ".ssh/.nixconfig";
in
{
  programs.ssh = {
    enable = true;

    controlMaster = "auto"; # Connect once then reuse existing session
    controlPath = lib.mkDefault "~/.ssh/.master-%C"; # Need to change default path to connect to looooong hostnames / IPv6 hosts
    controlPersist = lib.mkDefault "10m"; # Persist sessions for 10m then close them

    serverAliveInterval = 5;

    extraConfig = lib.strings.concatStringsSep "\n" [
      "User root"

      "StrictHostKeyChecking accept-new" # AutoAccept unknown HostKeys, but keep strict for k
      "VisualHostKey yes" # Display a visualisation of unknown HostKeys

      # "IdentityFile ~/.ssh/id_ed25519_sk_rk_S3NS" # YubiKey -> create file w/ ssh-keygen -K
      # "## IdentityAgent ~/.1password/agent.sock" # Use 1Password SSH Agent
      "PreferredAuthentications publickey" # see passh alias for login using Password/Interactive

      "ExitOnForwardFailure yes"

      # Legacy SSH
      # "PubkeyAcceptedKeyTypes +ssh-rsa"
      # "HostKeyAlgorithms +ssh-rsa"
    ];
  };

  # Correct permissions
  # FIXED: dont need that anymore but still here justincase
  # home.file."${configFile}" = {
  #   target = "${targetFile}";

  #   onChange = ''
  #     [[ ! -f $HOME/${configFile} ]] || rm -v $HOME/${configFile}; 
  #     { 
  #       echo -n "# NixOS Managed -> $(readlink $HOME/${targetFile})";
  #       cat $HOME/${targetFile};
  #     } > $HOME/${configFile} && chmod 400 $HOME/${configFile}
  #   '';
  # };
}
