{
  config,
  pkgs,
  lib,
  osConfig,
  ...
}:

{
  # TODO please change the username & home direcotry to your own
  # home.username = "toinux";
  # home.homeDirectory = "/home/toinux";

  imports = [
    ./packages

    ./git.nix
    ./ssh.nix
    ./zsh.nix
    ./vscode.nix
    ./kube.nix
  ];

  # link the configuration file in current directory to the specified location in home directory
  # home.file.".config/i3/wallpaper.jpg".source = ./wallpaper.jpg;

  # link all files in `./scripts` to `~/.config/i3/scripts`
  # home.file.".config/i3/scripts" = {
  #   source = ./scripts;
  #   recursive = true;   # link recursively
  #   executable = true;  # make all files executable
  # };

  home.sessionVariables = {
    EDITOR = "vim";
  };

  home.file.".xprofile".text = ''
    if [ -e $HOME/.profile ]
    then
    	. $HOME/.profile
    fi
  '';

  home.file.".config/discord/settings.json".text = builtins.toJSON { "SKIP_HOST_UPDATE" = true; };

  # alacritty - a cross-platform, GPU-accelerated terminal emulator
  programs.alacritty = {
    enable = true;

    # custom settings
    settings = {
      env.TERM = "xterm-256color";

      window = {
        opacity = 1.0;
        blur = true;
        resize_increments = true;
        dimensions = {
          columns = 0;
          lines = 0;
        };

        padding = {
          x = 2;
          y = 2;
        };
        decorations = "None";
      };

      keyboard = {
        bindings = [
          {
            key = "N";
            mods = "Command";
            action = "SpawnNewInstance";
          }
        ];
      };

      font = {
        normal = {
          family = "Hack Nerd Font Mono";
          style = "Regular";
        };

        # The bold font face
        bold = {
          family = "Hack Nerd Font Mono";
          style = "Bold";
        };

        # The italic font face
        italic = {
          family = "Hack Nerd Font Mono";
          style = "Italic";
        };

        size = 9;
      };
      scrolling.multiplier = 5;
      # selection.save_to_clipboard = true;
    };
  };

  # This value determines the home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update home Manager without changing this value. See
  # the home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "23.11";

  # Versions Dump
  home.file."current-home-packages".text =
    let
      getName = (p: if p ? name then "${p.name}" else "${p}");
      packages = builtins.map getName config.home.packages;
      sortedUnique = builtins.sort builtins.lessThan (lib.unique packages);
      formatted = builtins.concatStringsSep "\n" sortedUnique;
    in
    formatted;

  # Let home Manager install and manage itself.
  programs.home-manager.enable = true;
}
