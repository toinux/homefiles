{
  config,
  lib,
  pkgs,
  osConfig,
  ...
}:

{
  home.packages = with pkgs; [
    git
    tig
    delta
  ];

  programs.git = {
    enable = true;
    userName = "Antoine 'Toinux' Lesieur";
    userEmail = lib.mkDefault "toinux@${osConfig.networking.hostName}.local";
    aliases = {
      yolo = "push --force-with-lease";
      lg = "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit";
    };
    extraConfig = {
      core = {
        ignorecase = false;
        pager = "delta";
      };
      interactive = {
        diffFilter = "delta --color-only";
      };
      delta = {
        navigate = "true"; # use n and N to move between diff sections
        light = "false"; # set to true if you're in a terminal w/ a light background color (e.g. the default macOS terminal)
        side-by-side = "true";
        line-numbers = "true";
      };
      merge = {
        conflictstyle = "diff3";
      };
      diff = {
        colorMoved = "zebra";
      };
      pager = {
        branch = "false";
        tag = "false";
      };
      fetch = {
        prune = true;
      };
      pull = {
        rebase = true;
      };
      push = {
        autoSetupRemote = true;
      };

      url."git@github.com:".insteadOf = [ 
        "http://github.com/"
        "https://github.com/"
      ];
      url."git@gitlab.com:".insteadOf = [ 
        "http://gitlab.com/"
        "https://gitlab.com/"
      ];
    };
  };
}
