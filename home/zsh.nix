{
  lib,
  pkgs,
  config,
  osConfig,
  ...
}: {
  # starship - an customizable prompt for any shell
  programs.starship = {
    enable = true;
    # custom settings
    settings = {
      add_newline = false;
      aws.disabled = true;
      gcloud.disabled = true;
      username.show_always = true;
      # line_break.disabled = true;
    };
  };

  programs.thefuck.enable = true;
  programs.zoxide.enable = true;
  programs.atuin = {
    enable = true;
    enableZshIntegration = true;
    flags = [ "--disable-up-arrow" ];
  };

  programs.zsh = {
    enable = true;
    enableCompletion = true;

    defaultKeymap = "emacs";

    autocd = true;

    initExtra = lib.concatStringsSep "\n" (
      [
        "unsetopt complete_aliases"

        ''export PATH="$PATH:$HOME/bin:$HOME/.local/bin:$HOME/go/bin:$HOME/.krew/bin"''

        ''
          # compdef nixformat=nixfmt
          function nixformat() {
            find . -type f -name '*.nix' -print -exec nixfmt ''${@:-} -v {} \; ;
          }
        ''

        ''
          compdef renix=nixos-rebuild
          function renix() {
            sudo nixos-rebuild --show-trace ''${@:-switch} |& nom # Nix-Rebuild w/ proper output
          }

        ''

        ''
          compdef 1psock=sudo
          function 1psock() {
            _1PSOCK="$HOME/.1password/agent.sock";
            echo "SSH_AUTH_SOCK: $SSH_AUTH_SOCK -> $_1PSOCK";
            [[ $# -ge 1 ]] || return 1;
            ( set -x; SSH_AUTH_SOCK=$_1PSOCK $@; );
          };
        ''

        ''
          function nixtory() {
            local ARG=history
            if [[ "$1" == "--clean" ]]; then
              ARG=wipe-history
            fi

            local SUDO=()
            if [[ "$ARG" == "wipe-history" ]]; then
              SUDO=sudo;
            fi

            ''${SUDO:-} nix profile $ARG --profile /nix/var/nix/profiles/system;
            if [[ "$ARG" == "wipe-history" ]]; then
              ''${SUDO:-} nix-collect-garbage -d
            fi
          }

        ''

        ''
          sshKillall() { for f in ~/.ssh/*; do [ -S $f ] || continue; ssh -o ControlPath=$f -O exit _; done; }
        ''

        ''
          # pveversion for nix
          function nixversion(){ printf "# System Packages\n"; cat /etc/current-system-packages ; printf "\n\n# Home Packages\n"; cat "$HOME/current-home-packages"; echo; }
        ''
      ]
      ++ lib.optionals (config.home.kubenv.enable && !(config.home.sessionVariables ? KUBECONFIG)) [
        ''
          [[ -f "$KUBECONFIG" || ! -d "$HOME/.kube" ]] || export KUBECONFIG="$HOME/.kube/config:$(find $HOME/.kube/ -type f '(' -name '*.config' -o -name '*.kubeconfig' ')' | sed ':a;N;s/\n/:/;ba')"
          # echo "KubeConfig is : $KUBECONFIG"
        ''
      ]
    );

    syntaxHighlighting.enable = true;
    autosuggestion.enable = true;

    oh-my-zsh = {
      enable = true;

      plugins = [
        "git"
        "thefuck"
      ];
      theme = "";
    };

    historySubstringSearch.enable = true;
    history = {
      ignoreAllDups = true;
      ignoreSpace = true;

      ignorePatterns = [
        "rm *"
        "pkill *"

        # "[.]+" # Cd ....

        # "ccccc[:alnum:]+" # YubiKey fail
      ];

      extended = true;
    };

    # set some aliases, feel free to add more or remove some
    shellAliases = {
      rl = "exec $SHELL"; # Reload

      k = "kubectl";
      mv = "mv -vi"; # ask confirmation on dangerous operations
      rm = "rm -vI"; # ask confirmation on dangerous operations
      svim = "sudo vim";
      # cleanixtory = "nix profile wipe-history --profile /nix/var/nix/profiles/system";
      systemdpasswd = "sudo systemd-tty-ask-password-agent --query --watch";
      ovpn = "/run/current-system/sw/bin/openvpn_systemd.sh";

      passh = "ssh -o PreferredAuthentications=password,keyboard-interactive";
      "1pssh" = "1psock ssh -o IdentitiesOnly=no -o IdentityFile=/dev/null";

      #urldecode = "python3 -c 'import sys, urllib.parse as ul; print(ul.unquote_plus(sys.stdin.read()))'";
      #urlencode = "python3 -c 'import sys, urllib.parse as ul; print(ul.quote_plus(sys.stdin.read()))'";
    };

    plugins = [
      {
        name = "jq";
        # file = "init.sh";
        src = pkgs.fetchFromGitHub {
          owner = "reegnz";
          repo = "jq-zsh-plugin";
          rev = "48befbcd91229e48171d4aac5215da205c1f497e";

          sha256 =
            if true
            then "q/xQZ850kifmd8rCMW+aAEhuA43vB9ZAW22sss9e4SE="
            else lib.fakeSha256;
        };
      }
      {
        name = "zsh-fzf-tab";
        file = "fzf-tab.plugin.zsh";
        src = pkgs.fetchFromGitHub {
          owner = "Aloxaf";
          repo = "fzf-tab";
          rev = "v1.1.2";
          sha256 =
            if true
            then "0ymp9ky0jlkx9b63jajvpac5g3ll8snkf8q081g0yw42b9hwpiid"
            else lib.fakeSha256;
        };
      }
      {
        name = "zsh-nix-shell";
        file = "nix-shell.plugin.zsh";
        src = pkgs.fetchFromGitHub {
          owner = "chisui";
          repo = "zsh-nix-shell";
          rev = "v0.8.0";
          sha256 =
            if true
            then "1lzrn0n4fxfcgg65v0qhnj7wnybybqzs4adz7xsrkgmcsr0ii8b7"
            else lib.fakeSha256;
        };
      }
    ];
  };

  # home.sessionVariables = {  };
}
