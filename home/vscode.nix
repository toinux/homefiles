{
  pkgs,
  lib,
  config,
  ...
}: let
  cfg = config.programs.vscode;

  codePython = (
    pkgs.python3.withPackages (
      python-pkgs:
        with python-pkgs; [
          pip
          ipykernel
          # requests
        ]
    )
  );

  codePackages =
    [codePython]
    ++ (with pkgs; [
      nixfmt-rfc-style
      shfmt
      go
    ]);

  configFile = ".config/Code/User/settings.json";
  targetFile = ".config/Code/User/settings.nix.json";
in {
  config = lib.mkIf (cfg.enable) {
    home.packages =
      codePackages
      # ++ (with pkgs; [ ])
      ;

    # nix eval --expr 'builtins.fromJSON (builtins.readFile "'"$HOME/.config/Code/User/settings.json.orig"'")' --impure | nixfmt
    programs.vscode = {
      # enable = true;

      enableExtensionUpdateCheck = false;
      enableUpdateCheck = false;

      userSettings = {
        "C_Cpp.intelliSenseEngine" = "disabled";
        "[dockerfile]" = {
          "editor.defaultFormatter" = "ms-azuretools.vscode-docker";
        };
        "[nix]" = {
          "editor.defaultFormatter" = "jnoortheen.nix-ide";
        };
        "[shellscript]" = {
          "editor.formatOnSave" = false;
          "files.eol" = "\n";
        };
        "diffEditor.ignoreTrimWhitespace" = false;
        "editor.formatOnPaste" = false;
        "editor.formatOnType" = true;
        "editor.lineNumbers" = "relative";
        "extensions.autoCheckUpdates" = false;
        "files.insertFinalNewline" = true;
        "files.simpleDialog.enable" = true;
        "files.trimFinalNewlines" = true;
        "git.autofetch" = true;
        "nix.enableLanguageServer" = true;
        "nix.formatterPath" = "alejandra";
        "nix.serverPath" = "nixd";
        "nix.serverSettings" = {
          nixd = {
            formatting = {
              command = ["alejandra"];
            };
            options = {
              home-manager = {
                expr = "(import <home-manager/modules> { configuration = { home.username = \"root\"; home.stateVersion = \"24.05\"; home.homeDirectory = \"/root\"; }; pkgs = import <nixpkgs> {}; }).options";
              };
            };
          };
        };
        "terminal.integrated.enableVisualBell" = true;
        "terminal.integrated.scrollback" = 5000;
        "update.mode" = "none";
        "window.title" = "\${appName}\${separator}\${dirty}\${activeEditorShort}\${separator}\${rootName}\${separator}\${profileName}";
        "window.titleBarStyle" = "custom";
        "workbench.editorLargeFileConfirmation" = 10;
      };

      package = pkgs.vscode.fhsWithPackages (
        ps:
          (with ps; [
            zlib
            openssl.dev
            pkg-config
          ])
          ++ codePackages
      );

      extensions =
        pkgs.vscode-utils.extensionsFromVscodeMarketplace
        (import ./vscode_extensions.nix)
        .extensions;
    };

    # Settings UI deals badly with RO file-system, so we create a writable file then we import config
    home.file."${config.home.homeDirectory}/${configFile}" = {
      target = "${targetFile}";

      onChange = ''
        if [[  -f $HOME/${configFile} ]]; then
          if ! diff -q <(jq -r . $HOME/${configFile} || true) <(jq -r . $HOME/${targetFile}); then
            jq -r . $HOME/${configFile} > $HOME/${configFile}.orig;
          fi
        fi

        jq -r . $HOME/${targetFile} > $HOME/${configFile} && chmod 600 $HOME/${configFile}
      '';
    };
  };
}
