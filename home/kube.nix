{
  lib,
  pkgs,
  config,
  osConfig,
  ...
}:
with lib;

let
  cfg = config.home.kubenv;
in
{

  options.home.kubenv = {
    enable = mkEnableOption "enable Kube config";

    additionalKrewPlugins = mkOption {
      type = types.listOf types.str;
      default = [ ];
      example = [ "oidc-login" ];
      description = ''
        List of krew plugins to install.
      '';
    };
  };

  config = mkIf cfg.enable {
    # Kubernetes-related
    home.packages = with pkgs; [
      kubectl
      kustomize
      kubernetes-helm
      krew
      krewfile
      k9s
      crane
      fluxcd
      weave-gitops
      velero
    ];

    home.file.".kube/config".text = "";

    # home.file.".kube/switch-config.yaml".source =
    #   (pkgs.formats.yaml { }).generate "kubeswitch-config.yaml"
    #     { name = "toto"; };

    home.file.".krewfile" = {
      text = concatStringsSep "\n" (
        [
          "krew" # Mandatory
          "oidc-login"

          "ns"
          "ctx"

          "explore"
          "autons"
          "get-all"

          "neat"

          "community-images"
          "images"

          "access-matrix"
          "graph"
          "hns"
          "klock"
          "konfig"
          "kurt"
          "view-secret"
          "virt"
          "whoami"
          "colorize-applied"
          "service-tree"
        ]
        ++ cfg.additionalKrewPlugins
      );

      onChange = ''
        PATH="${config.home.path}/bin:${pkgs.krew}/bin:${pkgs.krewfile}/bin:/etc/profiles/per-user/$USER/bin:$PATH" $DRY_RUN_CMD bash -c 'krew update && krewfile -file $HOME/.krewfile -upgrade'
      '';
    };
  };
}
