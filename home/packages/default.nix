{
  pkgs,
  lib,
  config,
  osConfig,
  ...
}: {
  imports = [
    ./onlyX.nix
  ];

  # Packages that should be installed to the user profile.
  home.packages =
    with pkgs; [
      fastfetch

      # archives
      zip
      xz
      unzip
      p7zip

      # utils
      ripgrep # recursively searches directories for a regex pattern
      jq # A lightweight and flexible command-line JSON processor
      yq-go # yaml processer https://github.com/mikefarah/yq
      eza # A modern replacement for ‘ls’
      fzf # A command-line fuzzy finder
      cloc

      # Cryptenchiffré
      age
      sops
      gnupg

      # networking tools
      mtr # A network diagnostic tool
      iperf3
      socat # replacement of openbsd-netcat
      ipcalc # it is a calculator for the IPv4/v6 addresses
      nmap # A utility for network discovery and security auditing
      # dnsutils # `dig` + `nslookup`
      # ldns # replacement of `dig`, it provide the command `drill`
      q # next-gen DNS client

      # GNU Utils
      gnused
      gnutar
      gawk
      # misc
      tree
      which
      file
      zstd
      bat
      vim # TODO: drop it for program.vim
      tmux # TODO: drop it for program.tmux
      
      # nix related
      #
      # it provides the command `nom` works just like `nix
      # with more details log output
      nix-output-monitor
      nix-prefetch-git

      
      # system call monitoring
      strace # system call monitoring
      ltrace # library call monitoring
      lsof # list open files

      # system tools
      sysstat
      lm_sensors # for `sensors` command
      ethtool
      pciutils # lspci
      usbutils # lsusb
      ncdu # disk usage analyzing
    ];
}
