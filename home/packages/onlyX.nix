{ pkgs, lib, osConfig, ... }: {
  home.packages = lib.optionals (osConfig.services.xserver.enable) (with pkgs; [
    vesktop # Discord
    nnn # terminal file manager
    aria2 # A lightweight multi-protocol & multi-source command-line download utility
    minio-client
    cowsay
    mosh
  ]);
}
