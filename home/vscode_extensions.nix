{
  extensions = [
    {
      name = "nix";
      publisher = "bbenoist";
      version = "1.0.1";
      sha256 = "0zd0n9f5z1f0ckzfjr38xw2zzmcxg1gjrava7yahg5cvdcw6l35b";
    }
    {
      name = "nixfmt-vscode";
      publisher = "brettm12345";
      version = "0.0.1";
      sha256 = "07w35c69vk1l6vipnq3qfack36qcszqxn8j3v332bl0w6m02aa7k";
    }
    {
      name = "gotemplate";
      publisher = "casualjim";
      version = "0.5.0";
      sha256 = "19y2lwcv7024xidz20m63an79wv48drl7qwh7z30jqk24w4rd056";
    }
    {
      name = "gitlens";
      publisher = "eamodio";
      version = "2024.3.2605";
      sha256 = "00xzsccdgjyqs7dvc0pk51d2qnqvdr44irsqwfidwaxg6xn7h6i7";
    }
    {
      name = "shell-format";
      publisher = "foxundermoon";
      version = "7.2.5";
      sha256 = "0a874423xw7z6zjj7gzzl39jahrrqcf2r16zbcvncw23483m3yli";
    }
    {
      name = "vscode-docker";
      publisher = "ms-azuretools";
      version = "1.29.0";
      sha256 = "0rz32qwdf7a5hn3nnhxviaf8spwsszfrxmhnbbskspi5r9b6qm4r";
    }
    {
      name = "remote-containers";
      publisher = "ms-vscode-remote";
      version = "0.352.0";
      sha256 = "03nl7h4x9lgjlylv26hllk680jjjccdjii4c01gjy8dqq0jhrfbc";
    }
    {
      name = "remote-ssh";
      publisher = "ms-vscode-remote";
      version = "0.111.2024032515";
      sha256 = "1z90fhrinasaafr8ki38hd8bb57l6q3lp475kgp19lvnh2v2g79i";
    }
    {
      name = "remote-ssh-edit";
      publisher = "ms-vscode-remote";
      version = "0.86.0";
      sha256 = "0cmh2d73y1kmp6a92h3z7gams7lnqvb7rgib52kqslm4hyhdmii6";
    }
    {
      name = "remote-wsl";
      publisher = "ms-vscode-remote";
      version = "0.88.0";
      sha256 = "0825vpgj3jdy8n76zcqkj94w7n17dn7i98sks7h720zcqmj0hhdc";
    }
    {
      name = "vscode-remote-extensionpack";
      publisher = "ms-vscode-remote";
      version = "0.25.0";
      sha256 = "1nz4yx4s79lww4af88q3q8xxakzwdxva5sm7bffhilpqy5k8nmqa";
    }
    {
      name = "remote-explorer";
      publisher = "ms-vscode";
      version = "0.5.2024031109";
      sha256 = "1r8wdlz7p0k4kzfcmqiizns043lhghf5v34sf0gays02x8x9xh5p";
    }
    {
      name = "remote-server";
      publisher = "ms-vscode";
      version = "1.6.2024032609";
      sha256 = "1xjg82gzw50bikk3s9rl5hz65q6nbl4i58al110anf1nfkc45gy2";
    }
    {
      name = "vscode-xml";
      publisher = "redhat";
      version = "0.26.2024031808";
      sha256 = "1fs92qmw59078bm1n0kxdixllda7l36mbqs0kcx8clwfiiajz5dk";
    }
    {
      name = "vscode-yaml";
      publisher = "redhat";
      version = "1.14.0";
      sha256 = "0pww9qndd2vsizsibjsvscz9fbfx8srrj67x4vhmwr581q674944";
    }
    {
      name = "save-as-root";
      publisher = "yy0931";
      version = "1.8.0";
      sha256 = "11s4s9w2zrhxmbmxypjk88491zgi052mfxk60ayyjdqjisjvqyp3";
    }
  ];
}
