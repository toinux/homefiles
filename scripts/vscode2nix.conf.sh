#! /usr/bin/env nix-shell
#! nix-shell -i bash -p nix -p jq -p nixfmt-rfc-style -p alejandra
# shellcheck shell=bash
set -eu -o pipefail

cat <(grep -vE '^[^:"]+//' $HOME/.config/Code/User/settings.json) | nix eval --expr 'builtins.fromJSON (builtins.readFile '"/dev/stdin"')' --impure | nixfmt | alejandra --quiet
